//
//  ViewController.m
//  YelpNearby
//
//  Created by Behera, Subhransu on 12/5/13.
//  Copyright (c) 2013 Subh. All rights reserved.
//

#import "ViewController.h"
#import "Restaurant.h"
#import "ResultTableViewCell.h"

@interface ViewController ()

@end

const unsigned char SpeechKitApplicationKey[] = {0x0c, 0xe4, 0x01, 0xb0, 0xa3, 0xd3, 0x72, 0xfb, 0xac, 0x29, 0x85, 0x48, 0x6c, 0xe9, 0x9f, 0x6e, 0x0d, 0x51, 0x99, 0x80, 0xf6, 0xc8, 0x1e, 0x48, 0x3b, 0x5d, 0x6b, 0x83, 0xa9, 0x29, 0xb9, 0xb5, 0x0e, 0xba, 0x65, 0xb1, 0x8d, 0xd8, 0xac, 0x39, 0x01, 0x32, 0x82, 0x6d, 0x1b, 0x21, 0xdc, 0x29, 0x73, 0x75, 0xa9, 0x69, 0x3d, 0xc7, 0xb1, 0x78, 0x24, 0x1c, 0xfc, 0xce, 0x46, 0xb5, 0x83, 0x54};

@implementation ViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.messageLabel.text = @"Tap on the mic";
  self.activityIndicator.hidden = YES;
  
  if (!self.tableViewDisplayDataArray) {
    self.tableViewDisplayDataArray = [[NSMutableArray alloc] init];
  }
  
  self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
  [self.appDelegate updateCurrentLocation];
  [self.appDelegate setupSpeechKitConnection];
  
  self.searchTextField.returnKeyType = UIReturnKeySearch;
}

- (IBAction)recordButtonTapped:(id)sender {
  self.recordButton.selected = !self.recordButton.isSelected;
  
  // This will initialize a new speech recognizer instance
  if (self.recordButton.isSelected) {
    self.voiceSearch = [[SKRecognizer alloc] initWithType:SKSearchRecognizerType
                                                detection:SKShortEndOfSpeechDetection
                                                 language:@"en_US"
                                                 delegate:self];
  }
  
  // This will stop existing speech recognizer processes
  else {
    if (self.voiceSearch) {
      [self.voiceSearch stopRecording];
      [self.voiceSearch cancel];
    }
  }
}
# pragma mark - TableView Datasource and Delegate methods
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableViewDisplayDataArray count];
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ResultTableViewCell *cell = (ResultTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SearchResultTableViewCell"];
    
    Restaurant *restaurantObj = (Restaurant *)[self.tableViewDisplayDataArray objectAtIndex:indexPath.row];
    
    cell.nameLabel.text = restaurantObj.name;
    cell.addressLabel.text = restaurantObj.address;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *thumbImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:restaurantObj.thumbURL]];
        NSData *ratingImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:restaurantObj.ratingURL]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.thumbImage.image = [UIImage imageWithData:thumbImageData];
            cell.ratingImage.image = [UIImage imageWithData:ratingImageData];
        });
    });
    
    return cell;
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Restaurant *restaurantObj = (Restaurant *)[self.tableViewDisplayDataArray objectAtIndex:indexPath.row];
    
    if (restaurantObj.yelpURL) {
        UIApplication *app = [UIApplication sharedApplication];
        [app openURL:[NSURL URLWithString:restaurantObj.yelpURL]];
    }
}

# pragma mark - Textfield Delegate Method and Method to handle Button Touch-up Event

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.searchTextField isFirstResponder]) {
        [self.searchTextField resignFirstResponder];
    }
    
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([self.searchTextField isFirstResponder]) {
        [self.searchTextField resignFirstResponder];
    }
}

# pragma mark - when record button is tapped

# pragma mark - SKRecognizer Delegate Methods

- (void)recognizerDidBeginRecording:(SKRecognizer *)recognizer {
  self.messageLabel.text = @"Listening..";
}

- (void)recognizerDidFinishRecording:(SKRecognizer *)recognizer {
  self.messageLabel.text = @"Done Listening..";
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithResults:(SKRecognition *)results {
  long numOfResults = [results.results count];
  
  if (numOfResults > 0) {
    // update the text of text field with best result from SpeechKit
    self.searchTextField.text = [results firstResult];
  }
  
  self.recordButton.selected = !self.recordButton.isSelected;
  
  if (self.voiceSearch) {
    [self.voiceSearch cancel];
  }
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithError:(NSError *)error suggestion:(NSString *)suggestion {
  self.recordButton.selected = NO;
  self.messageLabel.text = @"Connection error";
  self.activityIndicator.hidden = YES;
  
  
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                  message:[error localizedDescription]
                                                 delegate:nil
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil];
  [alert show];
}
@end
