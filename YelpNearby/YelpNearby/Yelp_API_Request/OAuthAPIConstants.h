//
//  OAuthAPIConstants.h
//  YelpNearby
//
//  Created by Behera, Subhransu on 8/14/13.
//  Copyright (c) 2013 Behera, Subhransu. All rights reserved.
//

#ifndef YelpNearby_OAuthAPIConstants_h
#define YelpNearby_OAuthAPIConstants_h

#define OAUTH_CONSUMER_KEY @"YOUR_YELP_CONSUMER_KEY_HERE"
#define OAUTH_CONSUMER_SECRET @"YOUR_YELP_CONSUMER_SECRET_HERE"
#define OAUTH_TOKEN @"YOUR_OAUTH_TOKEN_HERE"
#define OAUTH_TOKEN_SECRET @"YOUR_TOKEN_SECRET_HERE"
#define YELP_SEARCH_URL @"http://api.yelp.com/v2/search"

#endif
